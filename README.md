# Covid-19

Covid-19 is single-page application (SPA) that shows the latest COVID-19 statistics feed (infected, recovered and deaths caused by Coronavirus) of the world and your country of choice in real-time, based on data from [Covid19 API](https://documenter.getpostman.com/view/10808728/SzS8rjbc?version=latest).

Demo: https://thanhdp.gitlab.io/covid-19/
## Data Source

- [Covid19 API](https://documenter.getpostman.com/view/10808728/SzS8rjbc?version=latest)

## Technology used in this project

- Vue.js
- Vuex
- Vue-router
- Axios
- Bootstrap-Vue
- Vue-Awesome

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Run test

```
npm run test
```

### Lints and fixes files

```
npm run lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/) and .env file
