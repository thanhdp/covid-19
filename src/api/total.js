import Axios from "../lib/Axios";
export default {
  fetchWorldData: function() {
    return Axios.get("world/total");
  },
  fetchCountryData: function(slug, fromDate, toDate) {
    if (!fromDate || !toDate) {
      toDate = new Date().toISOString().split("T")[0];
      fromDate = new Date();
      fromDate.setDate(fromDate.getDate() - 24);
      fromDate = fromDate.toISOString().split("T")[0];
    }
    return Axios.get(`total/country/${slug}?from=${fromDate}&to=${toDate}`);
  }
};
