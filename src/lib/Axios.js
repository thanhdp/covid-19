import Axios from "axios";

const axiosInstance = Axios.create({
  baseURL: process.env.VUE_APP_API
});

export default axiosInstance;
