const storage = window.localStorage;

export function setItem(key, value, expirationTime = null) {
  const now = new Date();
  if (expirationTime) {
    expirationTime = new Date(now.getTime() + expirationTime * 60 * 1000);
    expirationTime = expirationTime.getTime();
  }
  const data = {
    value: value === undefined ? null : value,
    createdAt: now.getTime(),
    expiresAt: expirationTime
  };
  storage.setItem(key, JSON.stringify(data));
}

export function getItem(key) {
  let data = storage.getItem(key);
  if (data) {
    const now = new Date();
    data = JSON.parse(data);
    if (!data.expiresAt || data.expiresAt >= now.getTime()) {
      return data.value;
    }
    storage.removeItem(key);
  }
  return undefined;
}

export function removeItem(key) {
  storage.removeItem(key);
}

export function clear() {
  storage.clear();
}

export default { setItem, getItem, removeItem, clear };
