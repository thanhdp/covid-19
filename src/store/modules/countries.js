import apiCountries from "../../api/countries";
import cache from "../../services/cache";

const state = {
  current: {
    name: "Global",
    slug: null
  },
  list: []
};
const mutations = {
  SET_CURRENT_COUNTRY(state, { slug }) {
    let currentCountry = null;
    if (slug) {
      currentCountry = state.list.find(country => country.Slug === slug);
    }
    state.current.name = currentCountry ? currentCountry.Country : "Global";
    state.current.slug = currentCountry ? currentCountry.Slug : null;
  },
  SET_COUNTRIES(state, { countries }) {
    state.list = countries;
    cache.setItem("countries", countries, 60 * 4);
  }
};
const actions = {
  fetchCountries: ({ commit, dispatch }, slug = null) => {
    if (!cache.getItem("countries")) {
      apiCountries
        .fetchList()
        .then(res => {
          commit("SET_COUNTRIES", {
            countries: res.data
          });
        })
        .then(() => {
          dispatch("setCurrentCountry", slug);
        })
        .catch(error => console.log(error));
    } else {
      commit("SET_COUNTRIES", {
        countries: cache.getItem("countries")
      });
    }
  },
  setCurrentCountry: ({ commit }, slug = null) => {
    commit("SET_CURRENT_COUNTRY", {
      slug
    });
  }
};
const getters = {
  currentCountry: state => {
    return state.current;
  },
  countryList: state => {
    return state.list;
  }
};
export default {
  state,
  mutations,
  actions,
  getters
};
