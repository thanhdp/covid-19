import apiSummary from "../../api/total";

const state = {
  data: {
    confirmed: 0,
    recovered: 0,
    deaths: 0
  },
  availableStats: true
};
const mutations = {
  SET_WORLD_DATA(state, { data }) {
    state.data.confirmed =
      data.TotalConfirmed !== undefined ? data.TotalConfirmed : 0;
    state.data.recovered =
      data.TotalRecovered !== undefined ? data.TotalRecovered : 0;
    state.data.deaths = data.TotalDeaths !== undefined ? data.TotalDeaths : 0;
  },
  SET_COUNTRY_DATA(state, { data }) {
    let lastData = {};
    if (data.length) {
      lastData = data[data.length - 1];
    }
    state.data.confirmed =
      lastData.Confirmed !== undefined ? lastData.Confirmed : 0;
    state.data.recovered =
      lastData.Recovered !== undefined ? lastData.Recovered : 0;
    state.data.deaths = lastData.Deaths !== undefined ? lastData.Deaths : 0;
  },
  SET_AVAILABLESTATS(state, { availableStats }) {
    state.availableStats = availableStats;
  }
};
const actions = {
  fetchCovidData: (
    { commit },
    countrySlug = null,
    fromDate = null,
    toDate = null
  ) => {
    commit("SET_AVAILABLESTATS", {
      availableStats: true
    });
    const summary =
      countrySlug == null
        ? apiSummary.fetchWorldData()
        : apiSummary.fetchCountryData(countrySlug, fromDate, toDate);
    summary
      .then(res => {
        countrySlug == null
          ? commit("SET_WORLD_DATA", { data: res.data })
          : commit("SET_COUNTRY_DATA", { data: res.data });
      })
      .catch(error => {
        if (error.response.status === 404) {
          commit("SET_AVAILABLESTATS", {
            availableStats: false
          });
        }
      });
  }
};
const getters = {
  covidData: state => {
    return state.data;
  },
  covidAvailableStats: state => {
    return state.availableStats;
  }
};
export default {
  state,
  mutations,
  actions,
  getters
};
