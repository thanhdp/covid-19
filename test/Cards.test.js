import { shallow } from "vue-test-utils";
import Cards from "../src/components/Cards";

describe("Cards.test.js", () => {
  let cmp;
  const covidData = {
    confirmed: 1000,
    recovered: 2000,
    deaths: 3000
  };

  beforeEach(() => {
    cmp = shallow(Cards, {
      propsData: {
        covidData: covidData
      }
    });
  });

  it("equals covidData to { confirmed: 1000, recovered: 2000, deaths: 3000 }", () => {
    expect(cmp.vm.covidData).toEqual(covidData);
  });

  it("has the expected html structure", () => {
    expect(cmp.element).toMatchSnapshot()
  });
});
