import { createLocalVue, shallow } from "vue-test-utils";
import Dropdown from "../src/components/countries/Dropdown";
import Vuex from "vuex";
import store from "../src/store";

describe("Dropdown.test.js", () => {
  let cmp;
  const localVue = createLocalVue();
  localVue.use(Vuex)
  const currentCountry = {
    Country: "Global",
    Slug: null
  };

  beforeEach(() => {
    cmp = shallow(Dropdown, {
      propsData: {
        currentCountry: currentCountry
      },
      store,
      localVue
    });
  });

  it("equals currentCountry to { Country: 'Global', Slug: null }", () => {
    expect(cmp.vm.currentCountry).toEqual(currentCountry);
  });

  it("has the expected html structure", () => {
    expect(cmp.currentCountry).toMatchSnapshot()
  });
});
